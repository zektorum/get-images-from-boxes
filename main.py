import json
import logging
import math
import os
import random

from wand.image import Image as WandImage

from image_types import Image


if __name__ == "__main__":
    logging.basicConfig(filename="processing.log", filemode="w", level=logging.DEBUG)
    path_to_dataset = "dataset/train"
    path_to_annotations = os.path.join(path_to_dataset, "_annotations.createml.json")
    path_to_output = "output"
    path_to_brake_on = "brake_on"
    path_to_brake_off = "brake_off"

    x_correction = 32
    y_correction = 45

    with open(path_to_annotations, "r") as file:
        raw_data = json.load(file)

    images = []
    for data in raw_data:
        images.append(Image(data))

    for image in images:
        logging.info(f"Processing image: {image.path}")
        max_box_resolution = 0
        index = 0
        max_resolution = ""
        for i, annotation in enumerate(image.annotations):
            resolution = f"{annotation.coordinates.width}x{annotation.coordinates.height}"
            logging.debug(f"Checking annotation #{i}")
            logging.debug(f"Box resolution: {resolution}")
            current_box_resolution = annotation.coordinates.width * annotation.coordinates.height
            if current_box_resolution > max_box_resolution:
                max_resolution = f"{annotation.coordinates.width}x{annotation.coordinates.height}"
                max_box_resolution = current_box_resolution
                index = i
        logging.debug(f"Max box resolution: {max_resolution} (annotation with index {index})")

        annotation = image.annotations[index]

        wand = WandImage(filename=os.path.join(path_to_dataset, image.path))

        source_image_resolution = wand.width * wand.height
        source_resolution = f"{wand.width}x{wand.height}"
        if max_box_resolution / source_image_resolution < 0.1:
            logging.info(f"Source image resolution: {source_resolution}")
            logging.info(f"Box image resolution: {max_resolution}")
            logging.info(f"Too small box. Skipping image...")
            continue

        wand.crop(
            math.floor(annotation.coordinates.x - x_correction),
            math.floor(annotation.coordinates.y - y_correction),
            width=math.floor(annotation.coordinates.width),
            height=math.floor(annotation.coordinates.height)
        )
        logging.debug(f"Box from '{image.path}' was cropped")

        if annotation.label == "car_BrakeOff":
            image_directory = path_to_brake_off
        else:
            image_directory = path_to_brake_on

        output_dir = os.path.join(path_to_output, image_directory)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
            logging.debug("Output directory doesn't exist. Creating...")

        wand.save(filename=os.path.join(output_dir, str(random.randint(0, 1000)) + image.path))
        logging.info(f"Cropped image '{image.path}' saved")
