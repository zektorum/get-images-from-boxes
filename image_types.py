from dataclasses import dataclass
from typing import Dict, List


@dataclass
class Coordinates:
    x: float
    y: float
    width: float
    height: float


@dataclass
class Annotation:
    label: str
    coordinates: Coordinates


@dataclass
class Image:
    path: str
    annotations: List[Annotation]

    def __init__(self, raw_data: Dict):
        self.path = raw_data["image"]
        self.annotations = [
            Annotation(item["label"], Coordinates(**item["coordinates"])) for item in raw_data["annotations"]
        ]
